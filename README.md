# Exposing Hazelcast bug #13456
Bug thread: https://github.com/hazelcast/hazelcast/issues/13456
## Steps to reproduce the bug
1. Create empty Eclipse project.
2. Import source files (folder src).
3. Add external jar `hazelcast-all-3.10.1.jar` to project's build path.
4. Open file `Main.java`. Make `IP_ADDRESS` equal the local IP.
5. Start `Main`.
6. Start `Main_two`.
7. Wait for the second instance to be up and running, then enter some input
   on `Main` to continue its execution.
8. Wait until the amount of entries differs from 10.000.
