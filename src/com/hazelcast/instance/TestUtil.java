package com.hazelcast.instance;
/*
 * Copyright (c) 2008-2018, Hazelcast, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.instance.HazelcastInstanceImpl;
import com.hazelcast.instance.HazelcastInstanceProxy;
import com.hazelcast.instance.Node;
import static java.lang.reflect.Proxy.isProxyClass;

@SuppressWarnings("WeakerAccess")
public final class TestUtil {

    private TestUtil() {
    }

    public static Node getNode(HazelcastInstance hz) {
        if (isProxyClass(hz.getClass())) {
            return null;// HazelcastStarter.getNode(hz);
        } else {
            HazelcastInstanceImpl hazelcastInstanceImpl = getHazelcastInstanceImpl(hz);
            return hazelcastInstanceImpl.node;
        }
    }

    public static HazelcastInstanceImpl getHazelcastInstanceImpl(HazelcastInstance hz) {
        if (hz instanceof HazelcastInstanceImpl) {
            return (HazelcastInstanceImpl) hz;
        } else if (hz instanceof HazelcastInstanceProxy) {
            HazelcastInstanceProxy proxy = (HazelcastInstanceProxy) hz;
            if (proxy.original != null) {
                return proxy.original;
            }
        }
        Class<? extends HazelcastInstance> clazz = hz.getClass();
        if (isProxyClass(clazz)) {
            return null;// HazelcastStarter.getHazelcastInstanceImpl(hz);
        }
        throw new IllegalArgumentException("The given HazelcastInstance is not an active HazelcastInstanceImpl: " + clazz);
    }

}
