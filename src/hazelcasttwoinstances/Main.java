package hazelcasttwoinstances;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapIndexConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Member;
import com.hazelcast.instance.TestUtil;
import com.hazelcast.internal.cluster.impl.ClusterServiceImpl;
import com.hazelcast.nio.Address;
import com.hazelcast.query.Predicates;
import com.hazelcast.spi.properties.GroupProperty;

public class Main {

	private static final List<String> POSSIBLE_VALUES = Arrays.asList("A", "B", "C", "D");
	private static final String IP_ADDRESS = "192.168.97.198";
	
	public static void main(String args[]) throws IOException {
		Config config = config();
		HazelcastInstance firstInstance = Hazelcast.newHazelcastInstance(config);
		BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
		
		
		for (long key = 0; key < 10000; key++) {
			String randomValue = POSSIBLE_VALUES.get(ThreadLocalRandom.current().nextInt(POSSIBLE_VALUES.size()));
			firstInstance.getMap("test").put(key, new TestObject(key, randomValue));
		}
		buffer.readLine();
		
		ClusterServiceImpl clusterService = TestUtil.getNode(firstInstance).getClusterService();
		Member suspectedMember = clusterService.getMember(new Address(IP_ADDRESS, 5702));
		clusterService.suspectMember(suspectedMember, null, true);
		//TestUtil.getNode(firstInstance).getConnectionManager().getConnection(new Address(IP_ADDRESS, 5702)).close("reason", null);
		
		
		while(true) {
			int size = firstInstance.getMap("test").keySet(Predicates.in("value", "A", "B", "C", "D")).size();
			System.out.println(size);
			if (size!=10000) break;
		}
	}

	protected static Config config() {
		return new Config()
				.setProperty(GroupProperty.MERGE_FIRST_RUN_DELAY_SECONDS.getName(), "5")
				.setProperty(GroupProperty.WAIT_SECONDS_BEFORE_JOIN.getName(), "0")
				.setInstanceName("main")
				.addMapConfig(new MapConfig()
						.setName("test")
						.addMapIndexConfig(new MapIndexConfig()
								.setAttribute("value").setOrdered(true)));
	}

	private static class TestObject implements Serializable {

		private Long id;
		private String value;

		public TestObject() {}

		public TestObject(Long id, String value) {
			this.id = id;
			this.value = value;
		}
	}
	
}
