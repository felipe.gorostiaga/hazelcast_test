package hazelcasttwoinstances;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapIndexConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.Predicates;
import com.hazelcast.spi.properties.GroupProperty;

public class Main_two {

	public static void main(String args[]) throws IOException {
		Config config = config();
		HazelcastInstance firstInstance = Hazelcast.newHazelcastInstance(config);
		BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
		
		buffer.readLine();
		Hazelcast.getAllHazelcastInstances();
		//System.out.println(secondInstance.getMap("test").keySet(Predicates.in("value", "A", "B", "C", "D")).size());
		//closeConnectionBetween(firstInstance, Hazelcast.getHazelcastInstanceByName("main"));
		boolean fstinst;
		while(true) {
			fstinst=true;
			int size = firstInstance.getMap("test").keySet(Predicates.in("value", "A", "B", "C", "D")).size();
			System.out.println(size);
			if (size!=10000) break;
		}
		System.out.println(fstinst);
	}

	protected static Config config() {
		return new Config()
				.setProperty(GroupProperty.MERGE_FIRST_RUN_DELAY_SECONDS.getName(), "5")
				.setProperty(GroupProperty.WAIT_SECONDS_BEFORE_JOIN.getName(), "0")
				.setInstanceName("main_two")
				.addMapConfig(new MapConfig()
						.setName("test")
						.addMapIndexConfig(new MapIndexConfig()
								.setAttribute("value").setOrdered(true)));
	}

	private static class TestObject implements Serializable {

		public TestObject() {}

		public TestObject(Long id, String value) {
		}
	}
	

	/*public static void closeConnectionBetween(HazelcastInstance h1, HazelcastInstance h2) {
		if (h1 == null || h2 == null) {
			return;
		}
		Node n1 = getNode(h1);
		Node n2 = getNode(h2);
		suspectMember(n1, n2);
		suspectMember(n2, n1);
	}

	public static Node getNode(HazelcastInstance hz) {
		return TestUtil.getNode(hz);
	}

	public static void suspectMember(Node suspectingNode, Node suspectedNode) {
		if (suspectingNode != null && suspectedNode != null) {
			ClusterServiceImpl clusterService = suspectingNode.getClusterService();
			Member suspectedMember = clusterService.getMember(suspectedNode.getLocalMember().getAddress());
			if (suspectedMember != null) {
				clusterService.suspectMember(suspectedMember, null, true);
			}
		}
	}*/

}
